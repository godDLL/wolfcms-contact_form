<?php

/* Security measure */
if (!defined('IN_CMS')) { exit(); }

class ContactFormController extends PluginController {

    public function __construct() {
        $this->setLayout('backend');
    }

    public function index() {
        $this->documentation();
    }

    public function documentation() {
        $this->display('contact_form/views/help');
    }

    function settings() {
        $this->display('contact_form/views/settings', Plugin::getAllSettings('contact_form'));
    }
}

