# Contact Form plugin for WolfCMS

Add a simple, or complex form easily anywhere on your website;  
with validation, complete message list in the Administration tab,  
flexible SMTP forwarding and more.


# Download

To get a stable version with all the latest changes [download :master](https://bitbucket.org/godDLL/wolfcms-contact_form/get/master.zip)

All [historical versions](https://bitbucket.org/godDLL/wolfcms-contact_form/downloads#tag-downloads) of the plugin are available.


# Installation

Make sure the folder you downloaded includes the file `index.php`
and is named exactly `contact_form`. Put it in your `wolf/plugins/`
folder, so that it sits at `wolf/plugins/contact_form/index.php`

In the Administration tab click on a checkbox to enable this plugin.


# Features

# TODO

## 0.1.0
- [ ] create a form anywhere in the tree (or turn on the Behaviour)
- [ ] configure the form in Administration (or edit the Page, adding inputs)
- [ ] collect validated results into the database, for each form
- [ ] per-form view, delete, forward as email or download as CSV
- [ ] full utf-8 support in message body and address fields
- [ ] email has the plaintext part, looks less spammy

## 0.2.0
- [ ] per-form forwarding email adress
- [ ] configurable per-form confirmation email for the form user


# Bugs and Improvements

When you see some issue, or have an idea for improvement please
search the [Issue tracker](https://bitbucket.org/godDLL/wolfcms-contact_form/issues), and possibly create an Issue.

If your idea or bug comes with working code, please [make a PR](https://bitbucket.org/godDLL/wolfcms-contact_form/pull-requests/) onto the `:develop` branch.

Maybe your idea is a recipe for how to do something cool with
Contact Form. In that case head over to the [Documentation](https://bitbucket.org/godDLL/wolfcms-contact_form/wiki/Home) pages,
and add a new [Recipe](https://bitbucket.org/godDLL/wolfcms-contact_form/wiki/F.%20A.%20Q.%20(advanced%20usage)).

Code license is GPLv2, where applicable.
