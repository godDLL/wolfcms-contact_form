<?php

/* Security measure */
if (!defined('IN_CMS')) { exit(); }

Plugin::setInfos(array(
    'id'          => 'contact_form',
    'title'       => __('Contact Form'),
    'description' => __('Simple unicode-aware contact form, sends email.'),
    'type'        => 'both',
    'version'     => '0.1.0',
    'license'     => 'GPL',
    'author'      => 'Yuli Che.',
    'require_wolf_version' => '0.8.3'
));

Plugin::addController('contact_form', __('Contact Form'), 'administrator', false);

