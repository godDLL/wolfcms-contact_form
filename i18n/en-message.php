<?php

/**
 * Wolf CMS skeleton plugin language file
 *
 * @package Translations
 */

return array(
    'Contact Form' => 'Contact Form',
    'Simple unicode-aware contact form, sends email.' => 'Simple unicode-aware contact form, sends email.'
);
